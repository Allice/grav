---
title: blog
menu: Blog
slug: blog
visible: true
admin:
    children_display_order: collection
content:
    items:
        - '@self.children'
    leading: 2
    columns: 2
    limit: 9
    order:
        by: date
        dir: desc
    show_date: true
    pagination: true
    url_taxonomy_filters: true
---

