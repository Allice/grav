---
title: cat-1
menu: cat-1
slug: cat-1
visible: true
admin:
    children_display_order: collection
content:
    items:
        - '@self.children'
    leading: 1
    columns: 2
    limit: 9
    order:
        by: date
        dir: desc
    show_date: true
    pagination: true
    url_taxonomy_filters: true
---

