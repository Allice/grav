---
title: cat-2
menu: cat-2
slug: cat-2
visible: true
admin:
    children_display_order: collection
content:
    items:
        - '@self.children'
    leading: 1
    columns: 2
    limit: 9
    order:
        by: date
        dir: desc
    show_date: true
    pagination: true
    url_taxonomy_filters: true
---

