---
title: page-title
menu: Blog-1
slug: blog
visible: true
admin:
    children_display_order: collection
content:
    items: '@self.children'
    leading: 2
    columns: 3
    limit: 11
    order:
        by: date
        dir: desc
    show_date: true
    pagination: true
    url_taxonomy_filters: true
---

